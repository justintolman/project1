//
//  ViewController.m
//  Project1
//
//  Created by Justin Tolman on 4/29/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import "MapViewController.h"
#import "ZSPinAnnotation.h"
#import "ZSAnnotation.h"
#import "PinFactory.h"

@interface MapViewController (){
    NSMutableArray *_pinArray;
    PinFactory *_pinBuilder;
}
@property (strong, nonatomic) IBOutlet MKMapView *projectMapView;
@end

@implementation MapViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _projectMapView.delegate=self;
	_pinArray = [[NSMutableArray alloc] init];
    _pinBuilder=[[PinFactory alloc] init];
    [_pinBuilder setMapViewController:self];
    [_pinBuilder netPinsJson:@"http://zstudiolabs.com/labs/compsci497/buildings.json"];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    //[self centerOnLocation];
}

- (IBAction)goToLocation:(id)sender {
    [self centerOnLocation];
}

-(void)centerOnLocation
{
    [_projectMapView setCenterCoordinate:_projectMapView.userLocation.coordinate animated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"addPin"]) {
        EditViewController *destViewController = segue.destinationViewController;
        [destViewController setLatitude:_projectMapView.userLocation.coordinate.latitude];
        [destViewController setLongitude:_projectMapView.userLocation.coordinate.longitude];
        [destViewController setMapViewController:self];
    }
}

-(void)addPin:(NSString *)title lat:(double)latitude long:(double)longitude color:(UIColor *)color desc:(NSString *)description type:(NSUInteger) typeNumber{
    ZSAnnotation *pin = [[ZSAnnotation alloc] init];
	pin.coordinate = CLLocationCoordinate2DMake(latitude, longitude);
	pin.color = color;
	pin.title = title;
	pin.subtitle = description;
    ZSPinAnnotationType annotationType;
    switch (typeNumber) {
        case 1:
            annotationType=ZSPinAnnotationTypeTag;
            break;
        case 2:
            annotationType=ZSPinAnnotationTypeDisc;
            break;
            
        default:
            annotationType=ZSPinAnnotationTypeStandard;
            break;
    }
    pin.type=annotationType;
	[_pinArray addObject:pin];
    _projectMapView.visibleMapRect = [self makeMapRectWithAnnotations:_pinArray];
	[_projectMapView addAnnotations:_pinArray];
}

-(void)removePin:(NSUInteger)index{
	[_pinArray removeObjectAtIndex:index];
}


- (MKMapRect)makeMapRectWithAnnotations:(NSArray *)annotations {
	
	MKMapRect flyTo = MKMapRectNull;
    for (id <MKAnnotation> annotation in annotations) {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0);
        if (MKMapRectIsNull(flyTo)) {
            flyTo = pointRect;
        } else {
            flyTo = MKMapRectUnion(flyTo, pointRect);
        }
    }
	
	return flyTo;
	
}


- (MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
	
    // Don't mess with user location
	if(![annotation isKindOfClass:[ZSAnnotation class]])
        return nil;
    
    ZSAnnotation *a = (ZSAnnotation *)annotation;
    static NSString *defaultPinID = @"StandardIdentifier";
    
    // Create the ZSPinAnnotation object and reuse it
    ZSPinAnnotation *pinView = (ZSPinAnnotation *)[_projectMapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
    if (pinView == nil){
        pinView = [[ZSPinAnnotation alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID];
    }
    
    // Set the type of pin to draw and the color
    pinView.annotationType = a.type;
    pinView.annotationColor = a.color;
    pinView.canShowCallout = YES;
    
    return pinView;
	
}



@end