//
//  EditViewController.h
//  Project1
//
//  Created by Justin Tolman on 4/29/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapViewController.h"
@class MapViewController;

@interface EditViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate>
-(void)setLatitude:(double)lat;
-(void)setLongitude:(double)lat;
-(void)setMapViewController:(MapViewController *)mvc;
@end