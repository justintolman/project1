//
//  PinFactory.h
//  Project1
//
//  Created by Justin Tolman on 4/30/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MapViewController.h"

@interface PinFactory : NSObject
-(void)setMapViewController:(MapViewController*)mvc;
-(void) netPinsJson: (NSString *)urlString;
@end
