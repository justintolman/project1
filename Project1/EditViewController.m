//
//  EditViewController.m
//  Project1
//
//  Created by Justin Tolman on 4/29/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import "EditViewController.h"

@interface EditViewController (){
    NSArray *colorArray;
    double _latitude;
    double _longitude;
    MapViewController *_MapViewController;
    
}
@property (strong, nonatomic) IBOutlet UITextField *titleInput;
@property (strong, nonatomic) IBOutlet UITextField *descriptionInput;
@property (strong, nonatomic) IBOutlet UILabel *colorDisplay;
@property (strong, nonatomic) IBOutlet UIPickerView *colorPicker;
@property (strong, nonatomic) IBOutlet UILabel *latitudeDisplay;
@property (strong, nonatomic) IBOutlet UILabel *longitudeDisplay;
@property (strong, nonatomic) IBOutlet UISegmentedControl *pinSelect;


@end

@implementation EditViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    colorArray=[[NSArray alloc] initWithObjects:@"Red",@"Orange",@"yellow",@"Green",@"Blue",@"Purple",@"Black",@"White", nil];
    // Do any additional setup after loading the view.
    
    _latitudeDisplay.text=[NSString stringWithFormat:@"%f",_latitude];
    _longitudeDisplay.text=[NSString stringWithFormat:@"%f",_longitude];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component
{
    return [colorArray count];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [colorArray objectAtIndex:row];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row   inComponent:(NSInteger)component{
    switch(row){
        case 0:
            _colorDisplay.backgroundColor=[UIColor redColor];
            break;
        case 1:
            _colorDisplay.backgroundColor=[UIColor orangeColor];
            break;
        case 2:
            _colorDisplay.backgroundColor=[UIColor yellowColor];
            break;
        case 3:
            _colorDisplay.backgroundColor=[UIColor greenColor];
            break;
        case 4:
            _colorDisplay.backgroundColor=[UIColor blueColor];
            break;
        case 5:
            _colorDisplay.backgroundColor=[UIColor purpleColor];
            break;
        case 6:
            _colorDisplay.backgroundColor=[UIColor blackColor];
            break;
        case 7:
            _colorDisplay.backgroundColor=[UIColor whiteColor];
            break;
    }
    
}

-(void)setLatitude:(double)lat
{
    _latitude=lat;
}

-(void)setLongitude:(double)lon
{
    _longitude=lon;
}

-(void)setMapViewController:(MapViewController *)mvc
{
    _MapViewController=mvc;
}

- (IBAction)createPin:(id)sender {
    [_MapViewController addPin:_titleInput.text lat:_latitude long:_longitude color:_colorDisplay.backgroundColor desc:_descriptionInput.text type:_pinSelect.selectedSegmentIndex];
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
