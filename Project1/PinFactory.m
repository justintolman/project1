//
//  PinFactory.m
//  Project1
//
//  Created by Justin Tolman on 4/30/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import "PinFactory.h"

@implementation PinFactory{
    MapViewController *_mapViewController;
}

-(void)setMapViewController:(MapViewController*)mvc{
    _mapViewController=mvc;
}

-(void) netPinsJson: (NSString *)urlString{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        for (NSDictionary *pin in responseObject) {
            [_mapViewController addPin:pin[@"name"] lat:[pin[@"location"][@"latitude"] doubleValue] long:[pin[@"location"][@"longitude"] doubleValue] color:[UIColor blueColor] desc:pin[@"description"] type:0];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"JSON failed to load." message:[error localizedDescription] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
    }];
    [operation start];
}

@end
